# Particle Filter 2D Localizer #

This application is a ROS node that utilizes a particle field to localize a robot in a known environment. This application was a class assignment where a framework was provided and I was tasked with building the missing pieces. The goal of this assignment was to write the functions needed to:

* Initialize the particle field (PF)
* Propagate the PF using odometry data
* Update the weights of the PF based on sensor readings

The initialization function was performed by continuously generating a random coordinate until one was found that is located inside of the map�s freespace. This was done for each particle. The orientation was also randomized.

The propagation step was performed by first calculating the robot's forward and angular velocities from the current and previous odometry data. This data was fed into the particle filter's move function where noise is added.

The update step was done in several parts. First, for each laser scan (only 10% of the total scans taken were used), the distance measured by the robot was compared to a ray trace simulated distance. The total scan deviation was stored for all scans used and for all particles. Next, the deviations were converted into weights by feeding them through a Gaussian function. Lastly, the weights were normalized with regard to the total number of particles.

Additionally, I updated the draw function to display the particle's color as a function of its weight. Brighter red indicated a larger weight.


To run the application:

* Modify "image: ...." with the correct path to the bitmap file pf2dlocalizer/maps/amoco_hall/amoco_hall.yaml
* In one terminal, run the command "rosbag play assignment_1b_no_map.bag"
* In another terminal,
    * export ROS_NAMESPACE=robot_1
    * run the command "rosrun pf2dlocalizer pf2dlocalizerNode _environment:=**path to the yaml file**"
    

Images can be seen below and a video can be seen on YouTube here: 
https://youtu.be/JXkM4k3I4Mc


![map1](images/map1.png)
![map2](images/map2.png)