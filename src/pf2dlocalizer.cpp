#include "pf2dlocalizer.h"

#include <fstream>
#include <iostream>

#include <boost/thread/mutex.hpp> // boost::mutex::scoped_lock
#include <angles/angles.h>        // normalize_angle, normalize_angle_positive

#include <unordered_map>
#include <list>
#include <tf/tf.h>

#define EPS 0.01

pf2dlocalizer::pf2dlocalizer(ros::NodeHandle &nh) {
    ROS_INFO("pf2dlocalizer::pf2dlocalizer ");
    ros::NodeHandle ph = ros::NodeHandle("~");

    std::string odom_topic, scan_topic, environment;
    double vSigma, wSigma;
    double sensor_accuracy, processed_scans_percentage;
    bool consider_inf_scans, use_scans;
    int pf_size;

    // Topics to listen
    ph.param<std::string>("odom_topic", odom_topic, "odom");
    ph.param<std::string>("scan_topic", scan_topic, "scan");
    ph.param<bool>("use_scans", use_scans, true);

    // Environment and context
    ph.param<std::string>("environment", environment, "amoco_hall.yaml");

    // Motion sigmas
    ph.param<double>("vSigma", vSigma, 0.5);
    ph.param<double>("wSigma", wSigma, 0.01);

    // Laser sensor parameters
    ph.param<double>("sensor_accuracy", sensor_accuracy, 0.3);
    ph.param<double>("processed_scans_percentage", processed_scans_percentage, 0.1);
    ph.param<bool>("consider_inf_scans", consider_inf_scans, true);

    // Particle filter parameters
    ph.param<int>("pf_size", pf_size, 1000);
    ph.param<double>("ESS_threshold", ESS_threshold_, 0.8);

    // Propagation and update thresholds
    ph.param<double>("update_min_d", d_thresh_, 0.4);
    ph.param<double>("update_min_a", a_thresh_, 0.2);

    ROS_INFO_STREAM("SETTING: environment " << environment);
    ROS_INFO_STREAM("Motion: vSigma " << vSigma << " wSigma " << wSigma);
    ROS_INFO_STREAM("Sensor: sensor_accuracy " << sensor_accuracy << " processed_scans_percentage "
                                               << processed_scans_percentage << " consider_inf_scans "
                                               << consider_inf_scans);
    ROS_INFO_STREAM("PF: size " << pf_size << " ESS_threshold " << ESS_threshold_ << " update_min_d " << d_thresh_
                                << " update_min_a " << a_thresh_);

    // Initialization of the particle filter
    ROS_INFO_STREAM("Uniform initialization");
    particle_filter_ = new pf(pf_size, environment, sensor_accuracy, processed_scans_percentage, consider_inf_scans,
                              vSigma, wSigma);

    // Subscribing to sensor topics
    odomSub = nh.subscribe(odom_topic, 1, &pf2dlocalizer::handleOdom, this);
    init_odom_ = false;
    if (use_scans) {
        ROS_INFO_STREAM("using scan");
        laserSub = nh.subscribe(scan_topic, 1, &pf2dlocalizer::handleScan, this);
        init_laser_ = false;
    }
}


wpose prev_pose;
ros::Time prev_time;
float dist_travelled, dist_turned;

void pf2dlocalizer::handleOdom(const nav_msgs::Odometry::ConstPtr &odom) {
    // TODO: Handle odom data for the propagation step
    // Hint : call particle_filter_->move(...)
    // Hint : Also update the flag for laser update, which serves for decreasing
    // the computational burden. The flag is true, if since the last update
    // the robot has traveled more than d_thresh_ or rotated more than a_thresh_
    //////////////////// ANSWER CODE BEGIN //////////////////////

    if (prev_time == ros::Time(0)) {
        prev_time = odom->header.stamp;
    } else {

        // get heading
        double roll, pitch, heading;
        tf::Quaternion q = tf::Quaternion(odom->pose.pose.orientation.x, \
            odom->pose.pose.orientation.y, odom->pose.pose.orientation.z, \
            odom->pose.pose.orientation.w);
        tf::Matrix3x3(q).getRPY(roll, pitch, heading);


        double distX = odom->pose.pose.position.x - prev_pose.x(),
                distY = odom->pose.pose.position.y - prev_pose.y(),
                distTheta = heading - prev_pose.theta();

        cout << "Prev Pose: X: " << prev_pose.x() << " Y: " << prev_pose.y() << " theta: " << prev_pose.theta() << endl;
        cout << "Odom Pose: X: " << odom->pose.pose.position.x << " Y: " << odom->pose.pose.position.y << " theta: "
             << heading << endl;
        cout << "Movement: X: " << distX << " Y: " << distY << " theta: " << distTheta << endl;


        double dt = (odom->header.stamp - prev_time).toSec();
        double dist = cv::norm(std::vector<double>{distX, distY});
        double v = dist / dt;
        double w = distTheta / dt;

        cout << "Movement: dist: " << dist << " Theta: " << distTheta << " dt: "
             << (odom->header.stamp - prev_time).toSec() << endl;

        cout << "Odom Pose: X: " << odom->pose.pose.position.x << " Y: " << odom->pose.pose.position.y << " theta: "
             << heading << endl;

        cout << "Calc Pose: X: " << prev_pose.x() + v * dt * cos(prev_pose.theta() + w) << " Y: "
             << prev_pose.y() + v * dt * sin(prev_pose.theta() + w) << " Theta: "
             << prev_pose.theta() + w * dt << endl;

        particle_filter_->move(v, w, dt);


        if (dist_travelled >= d_thresh_ || dist_turned >= a_thresh_) {
            update_ = true;
            dist_travelled = 0;
            dist_turned = 0;
        } else {
            dist_travelled += dist;
            dist_turned += distTheta;
        }

        prev_pose.setX(odom->pose.pose.position.x);
        prev_pose.setY(odom->pose.pose.position.y);
        prev_pose.setTheta(heading);
        prev_time = odom->header.stamp;
    }

    //////////////////// ANSWER CODE END //////////////////////

    // Update the particle filter just checking the map
    particle_filter_->update();

    // Resampling
    double cv2 = 0.0;
    int ESS = particle_filter_->ESS(cv2);
    std::cout << "cv2=" << cv2 << " ESS=" << ESS << std::endl;
    if (ESS < ESS_threshold_ * particle_filter_->size()) {
        particle_filter_->Resample();
    }

    // Output
    particle_filter_->draw();
    particle_filter_->StatWeight("huh");
}

void pf2dlocalizer::handleScan(const sensor_msgs::LaserScan::ConstPtr &msg) {
    // Update that happens according to d_thresh and a_thresh
    if (!init_laser_ || update_) {
        init_laser_ = true;

        // Update step
        particle_filter_->update_uniform(msg->ranges, msg->angle_min, msg->angle_increment, msg->range_max);

        // Resampling
        double cv2 = 0.0;
        int ESS = particle_filter_->ESS(cv2);
        if (ESS < ESS_threshold_ * particle_filter_->size()) {
            particle_filter_->Resample();
        }

        // Output
        particle_filter_->draw();
        particle_filter_->StatWeight("huh");

        // Setting back to false the laser update flag
        update_ = false;
    }
}
